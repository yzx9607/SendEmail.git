package com.yang.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;


@RestController
@RequestMapping("/sendEmail")
public class SendEmailController {
    @Autowired
    private JavaMailSender mailSender;

    /**
     * 发送文字邮件
     * @param content
     * @return
     */
    @RequestMapping(value = "/send", method = RequestMethod.GET)
    public String send(@RequestParam("content") String content) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setFrom("yzx9607@163.com");
            helper.setSubject("Java程序发送邮件");
            helper.setText(content);
            //设置多个收件人地址 多个地址可以通过,隔开，详细参考new InternetAddress().parse方法
            InternetAddress[] internetAddressTo = new InternetAddress().parse("yzx9607@qq.com,yangzx3@asiainfo.com");
            mimeMessage.setRecipients(MimeMessage.RecipientType.TO,internetAddressTo);
            //设置多个抄送人地址 多个地址可以通过,隔开，详细参考new InternetAddress().parse方法
            InternetAddress[] internetAddressCC = new InternetAddress().parse("liuzx3@asiainfo.com,heshuang5@asiainfo.com");
            mimeMessage.setRecipients(MimeMessage.RecipientType.CC,internetAddressCC);
            mimeMessage.saveChanges();
            mailSender.send(mimeMessage);
            return "发送成功";
        }catch (Exception e){
            e.printStackTrace();
            return "发送失败";
        }
    }

    /**
     * 发送携带附件的邮件
     * @return
     * @throws MessagingException
     */
    @RequestMapping("/sendFile")
    public String sendFile() throws MessagingException{
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom("yzx9607@163.com");
        helper.setTo("yzx9607@qq.com");
        helper.setSubject("主题：发送附件邮件");
        helper.setText("请查看附件。");
        FileSystemResource file1 = new FileSystemResource(new File("d:\\a.txt"));
        FileSystemResource file2 = new FileSystemResource(new File("d:\\b.txt"));
        helper.addAttachment("省份SQL.txt", file1);
        helper.addAttachment("市级SQL.txt", file2);
        try {
            mailSender.send(mimeMessage);
            return "带附件的邮件已发送。";
        } catch (Exception e) {
            return "带附件的邮件发送失败。";
        }
}
    /**
     * hello
     * @return
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String hello() {
        return "hello";
    }



}